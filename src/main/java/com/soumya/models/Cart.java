package com.soumya.models;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<CartItem> items = new ArrayList<>();

    public List<CartItem> getItems() {
        return items;
    }

    public void add(Product p, int quantity) {
        items.add(new CartItem(p, quantity));
    }
}
