package com.soumya.models;

public class CartItem {
    private Product product;
    private int quantity;
    private boolean promoApplied;

    public CartItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean isPromoApplied() {
        return promoApplied;
    }

    public void setPromoApplied(boolean promoApplied) {
        this.promoApplied = promoApplied;
    }

    public int getTotal() {
        return this.product.getUnitPrice() * this.quantity;
    }
}
