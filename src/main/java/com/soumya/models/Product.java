package com.soumya.models;

public class Product {
    private char sku;
    private int unitPrice;

    public Product(char sku, int unitPrice) {
        this.sku = sku;
        this.unitPrice = unitPrice;
    }

    public char getSku() {
        return sku;
    }

    public int getUnitPrice() {
        return unitPrice;
    }
}
