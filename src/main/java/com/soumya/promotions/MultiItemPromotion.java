package com.soumya.promotions;

import com.soumya.models.CartItem;

import java.util.Map;

public class MultiItemPromotion implements Promotion {
    private final Character sku1;
    private final Character sku2;
    private final int price;

    public MultiItemPromotion(Character sku1, Character sku2, int price) {
        this.sku1 = sku1;
        this.sku2 = sku2;
        this.price = price;
    }

    private int getDiscountedTotal(CartItem cartItem1, CartItem cartItem2) {
        if (cartItem1 == null || cartItem2 == null) {
            return 0;
        }

        if (cartItem1.isPromoApplied() || cartItem2.isPromoApplied()) {
            return 0;
        }

        int unitPrice1 = cartItem1.getProduct().getUnitPrice();
        int quantity1 = cartItem1.getQuantity();

        int unitPrice2 = cartItem2.getProduct().getUnitPrice();
        int quantity2 = cartItem2.getQuantity();

        int comboCount = Math.max(quantity1 - (quantity1 - quantity2), quantity2 - (quantity2 - quantity1));

        int comboPrice = comboCount * price;
        int leftOverPrice = ((quantity1 - comboCount) * unitPrice1) + ((quantity2 - comboCount) * unitPrice2);

        int res = comboPrice + leftOverPrice;

        cartItem1.setPromoApplied(true);
        cartItem2.setPromoApplied(true);

        return res;
    }

    @Override
    public int getDiscountedTotal(Map<Character, CartItem> skuCartMap) {
        return getDiscountedTotal(skuCartMap.get(sku1), skuCartMap.get(sku2));
    }
}
