package com.soumya.promotions;

import com.soumya.models.CartItem;

import java.util.Map;

public class CountBasedPromotion implements Promotion {
    private final Character sku;
    private final int count;
    private final int price;

    public CountBasedPromotion(Character sku, int count, int price) {
        this.sku = sku;
        this.count = count;
        this.price = price;
    }

    private int getDiscountedTotal(CartItem cartItem) {
        if (cartItem == null || cartItem.isPromoApplied()) {
            return 0;
        }

        int unitPrice = cartItem.getProduct().getUnitPrice();
        int quantity = cartItem.getQuantity();

        if (quantity < count) {
            return 0;
        }

        int setCount = quantity / count;
        int leftOver = quantity % count;

        int res = (setCount * price) + (unitPrice * leftOver);

        cartItem.setPromoApplied(true);
        return res;
    }

    @Override
    public int getDiscountedTotal(Map<Character, CartItem> skuCartMap) {
        return getDiscountedTotal(skuCartMap.get(sku));
    }
}
