package com.soumya.promotions;

import com.soumya.models.CartItem;

import java.util.Map;

public interface Promotion {
    int getDiscountedTotal(Map<Character, CartItem> skuCartMap);
}
