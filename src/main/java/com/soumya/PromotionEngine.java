package com.soumya;

import com.soumya.models.Cart;
import com.soumya.models.CartItem;
import com.soumya.promotions.CountBasedPromotion;
import com.soumya.promotions.MultiItemPromotion;
import com.soumya.promotions.Promotion;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PromotionEngine {
    private List<Promotion> promotions = new ArrayList<>();

    public PromotionEngine() {
        // 3 A's for 130
        promotions.add(new CountBasedPromotion('A', 3, 130));

        // 2 B's for 45
        promotions.add(new CountBasedPromotion('B', 2, 45));

        // C and D for 30
        promotions.add(new MultiItemPromotion('C', 'D', 30));
    }

    public int getCartTotal(Cart cart) {
        Map<Character, CartItem> skuCartMap = new HashMap<>();
        cart.getItems().forEach(cartItem -> skuCartMap.put(cartItem.getProduct().getSku(), cartItem));

        int promoItemsTotal = promotions.stream().map(
                promotion -> promotion.getDiscountedTotal(skuCartMap)
        ).mapToInt(Integer::intValue).sum();

        int nonPromoItemsTotal = cart.getItems().stream().filter(
                item -> !item.isPromoApplied()
        ).map(CartItem::getTotal).mapToInt(Integer::intValue).sum();

        return nonPromoItemsTotal + promoItemsTotal;
    }

}
