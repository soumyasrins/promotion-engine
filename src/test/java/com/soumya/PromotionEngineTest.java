package com.soumya;

import com.soumya.models.Cart;
import com.soumya.models.Product;
import org.junit.Test;

public class PromotionEngineTest {
    private static final Product a = new Product('A', 50);
    private static final Product b = new Product('B', 30);
    private static final Product c = new Product('C', 20);
    private static final Product d = new Product('D', 15);

    private static final PromotionEngine engine = new PromotionEngine();

    @Test
    public void testScenarioA() {
        Cart cart = new Cart();
        cart.add(a, 1);
        cart.add(b, 1);
        cart.add(c, 1);
        int cartTotal = engine.getCartTotal(cart);
        assert cartTotal == 100;
    }

    @Test
    public void testScenarioB() {
        Cart cart = new Cart();
        cart.add(a, 5);
        cart.add(b, 5);
        cart.add(c, 1);
        int cartTotal = engine.getCartTotal(cart);
        assert cartTotal == 370;
    }

    @Test
    public void testScenarioC() {
        Cart cart = new Cart();
        cart.add(a, 3);
        cart.add(b, 5);
        cart.add(c, 1);
        cart.add(d, 1);
        int cartTotal = engine.getCartTotal(cart);
        assert cartTotal == 280;
    }
}
